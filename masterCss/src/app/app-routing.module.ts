import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TdfComponent } from './components/tdf/tdf.component';
import { ReactiveComponent } from './components/reactive/reactive.component';


const routes: Routes = [

  { path:'template-driven-form', component : TdfComponent },
  { path:'reactive-form', component : ReactiveComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
